---
id: 7782
title: Tidur Terlalu Kesiangan
date: 2020-08-31T20:05:42+07:00
author: Redaksi
layout: post
guid: https://wildanfauzy.com/?p=7782
permalink: /tidur-terlalu-kesiangan/
header-img: wp-content/uploads/2020/08/tidur-kesiangan.jpg
header-mask: 0.4
categories:
  - Note
format: quote
---
<blockquote class="wp-block-quote">
  <p>
    Sudah sewajarnya terlalu lama tidur lebih dari delapan jam membuat kepala pusing, tapi anehnya jika kurang tidur tidak membuat kepala pusing hanya sedikit ngantuk setelah meminum air putih dicampur kopi maka akan segar kembali, besoknya tidur dengan pulas kembali ke siklus pertama jika terlalu banyak tidur kepala jadi pusing, jangan lupa menyiram tanaman.
  </p>
</blockquote>