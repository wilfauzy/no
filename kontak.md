---
id: 4700
title: Kontak
date: 2020-06-30T16:20:51+07:00
author: Redaksi
layout: page
description: Tak kenal maka tak tendang, hubungi kami.
header-img: "img/kontak.jpg"
header-mask: "0.4"
guid: https://wildanfauzy.com/?page_id=4700

---
## **PENERBIT** 

PT. WILBLOG SENTOSA JAYA JENAKA

## **ALAMAT**

<div itemprop="location" itemscope itemtype="http://schema.org/Organization" class="wp-block-jetpack-contact-info">
  <p>
    Jl. Merdeka No.53, Babakan, Kec. Ciwaringin, Cirebon, Jawa Barat 45167
  </p>
  
  <p>
    Cirebon ,&nbsp;Jawa Barat&nbsp;45167 Indonesia (<a href="https://maps.app.goo.gl/VbBgPihm5USbnTHW8" target="_blank" rel="noreferrer noopener">Peta</a>)
  </p>
</div>

<div itemprop="location" itemscope itemtype="http://schema.org/Organization" class="wp-block-jetpack-contact-info">
  <h2>
    <strong>EMAIL</strong>
  </h2>
  
  <ul>
    <li>
      Redaksi email: <a href="mailto:redaksi@wildanfauzy.com">redaksi@wildanfauzy.com</a>
    </li>
    <li>
      Kontak email: <a href="mailto:admin@wildanfauzy.com">admin@wildanfauzy.com</a>
    </li>
    <li>
      Report abuse: <a href="mailto:abuse@wildanfauzy.com">abuse@wildanfauzy.com</a>
    </li>
  </ul>
  
<h2>FORM KONTAK</h2>

<form id="my-form"
  action="https://formspree.io/f/mjvprlba"
  method="POST"
>
  <label>Email:</label>
  <input type="email" name="email" />
  <br><label>Pesan:</label>
  <input type="text" name="message" />
  <button id="my-form-button">Submit</button>
  <p id="my-form-status"></p>
</form>

<!-- Place this script at the end of the body tag -->

<script>
  window.addEventListener("DOMContentLoaded", function() {

    // get the form elements defined in your form HTML above
    
    var form = document.getElementById("my-form");
    var button = document.getElementById("my-form-button");
    var status = document.getElementById("my-form-status");

    // Success and Error functions for after the form is submitted
    
    function success() {
      form.reset();
      button.style = "display: none ";
      status.innerHTML = "Terima Kasih :)";
    }

    function error() {
      status.innerHTML = "Oops! Sepertinya ada masalah.";
    }

    // handle the form submission event

    form.addEventListener("submit", function(ev) {
      ev.preventDefault();
      var data = new FormData(form);
      ajax(form.method, form.action, data, success, error);
    });
  });
  
  // helper function for sending an AJAX request

  function ajax(method, url, data, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function() {
      if (xhr.readyState !== XMLHttpRequest.DONE) return;
      if (xhr.status === 200) {
        success(xhr.response, xhr.responseType);
      } else {
        error(xhr.status, xhr.response, xhr.responseType);
      }
    };
    xhr.send(data);
  }
</script>
